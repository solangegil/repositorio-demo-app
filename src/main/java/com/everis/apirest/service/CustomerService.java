package com.everis.apirest.service;

import com.everis.apirest.model.entity.Customer;

public interface CustomerService {
	
	
	public Iterable<Customer> obtenerCustomers();
	
	public Customer insertar(Customer customer);

}
