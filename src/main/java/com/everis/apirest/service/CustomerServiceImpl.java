package com.everis.apirest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.apirest.model.entity.Customer;
import com.everis.apirest.model.repository.CustomerRepository;


@Service
public class CustomerServiceImpl implements CustomerService{
	
	
	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public Iterable<Customer> obtenerCustomers() {
		// TODO Auto-generated method stub
		return customerRepository.findAll();
	}

	@Override
	public Customer insertar(Customer customer) {
		// TODO Auto-generated method stub
		return null;
	}
	
}

