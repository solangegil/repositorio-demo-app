package com.everis.apirest.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.apirest.controller.resource.CustomerResource;
import com.everis.apirest.service.CustomerService;

//import com.everis.apirest.controller.resource.ProductoResource;
//import com.everis.apirest.service.ProductoService;

@RestController
public class ApiController {
	
//	@Autowired
//	ProductoService productoService;
//	
//	@GetMapping("/productos")
//	public List<ProductoResource> obtenerProductos(){
//		
//		List<ProductoResource> listado = new ArrayList<>();
//		productoService.obtenerProductos().forEach(producto ->{
//			ProductoResource productoResource =  new ProductoResource();
//			productoResource.setId(producto.getId());
//			productoResource.setName(producto.getName());
//			productoResource.setDescription(producto.getDescription());
//			listado.add(productoResource);
//		});
//		
//		return listado;
//	}
	
	@Autowired
	CustomerService customerService;
	
	@GetMapping("/customers")
	public List<CustomerResource> obtenerCustomers(){
		
		List<CustomerResource> listado = new ArrayList<>();
		customerService.obtenerCustomers().forEach(customer ->{
			CustomerResource customerResource =  new CustomerResource();
			customerResource.setId(customer.getId());
			customerResource.setNombre(customer.getName());
			customerResource.setApellidoCompleto(customer.getLastName()+ " " + customer.getLastName2());
			listado.add(customerResource);
		});
		
		return listado;
	}
}
