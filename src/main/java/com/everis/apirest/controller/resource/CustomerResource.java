package com.everis.apirest.controller.resource;

import lombok.Data;

@Data
public class CustomerResource {
	
	private Long id;
	private String nombre;
	private String apellidoCompleto;
	

}
