package com.everis.apirest.controller.resource;

import lombok.Data;

@Data
public class ProductoResource {
	
	private Long id;
	private String name;
	private String description;

}
